@regression @navigation
Feature: Back To Top

  Scenario: Page is scrolled to the top when Back To Top button is pressed
    When back To Top button at the bottom of home page is pressed
    Then home page is scrolled to the top