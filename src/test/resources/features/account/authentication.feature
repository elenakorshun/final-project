@smoke @authentication
Feature: Authentication

  Scenario: Successful log in
    When login page is opened
    And email is entered on login page
    And password is entered on login page
    Then home page should be opened
    And default greeting message for guest should not be displayed on home page
    And sign in button should not be visible on home page
    And sign out button should be visible on home page

  Scenario: Successful log out
    Given user is logged in
    When sign out button is pressed on home page
    Then login page should be opened
    And default greeting message for guest should be displayed on home page