@regression @address
Feature: Adding addresses to account

  Background:
    Given user is logged in
    And Your Account page is opened
    And Your Addresses page is opened
    And addresses list is empty except for residential address

  Scenario: add 1 new address to account with all fields entered
    When addresses are added:
      | country       | fullName       | phoneNumber   | addressLine1 | addressLine2 | city     | state | zipCode    | isDefaultAddress |
      | US            | Elena Korshun1 | 123453454     |  1 BEARD ST  |  apt. 1      | BROOKLYN | NY    | 11231-1548 | false            |
    Then 1 address is appeared in address list(except for residential address)

  Scenario: add 1 new address to account with only required fields entered
    When addresses are added:
      | country       | fullName  | phoneNumber   |addressLine1 | addressLine2 | city     | state | zipCode    | isDefaultAddress |
      | US            |           | 123453454     | 1 BEARD ST  |              | BROOKLYN | NY    | 11231-1548 | true             |
    Then 1 address is appeared in address list(except for residential address)

  Scenario: add 2 new addresses to account
    When addresses are added:
      | country       | fullName       | phoneNumber   | addressLine1 | addressLine2 | city     | state | zipCode    | isDefaultAddress |
      | US            | Elena Korshun1 | 123453454     | 1 BEARD ST   | apt. 1       | BROOKLYN | NY    | 11231-1548 | false            |
      | US            | Elena Korshun2 | 123453454     | 1 BEARD ST   | apt. 1       | BROOKLYN | NY    | 11231-1548 | false            |
    Then 2 addresses are appeared in address list(except for residential address)

  Scenario: when new default address is added than old default address is no longer default(only one address can be default)
    When addresses are added:
      | country       |fullName   | phoneNumber   | addressLine1 |addressLine2 | city     | state | zipCode    | isDefaultAddress |
      | US            | FullName1 | 123453454     | 1 BEARD ST   | apt. 1      | BROOKLYN | NY    | 11231-1548 | true             |
      | US            | FullName2 | 123453454     | 1 BEARD ST   | apt. 1      | BROOKLYN | NY    | 11231-1548 | true             |
    Then Set as Default button is visible for firstly added address
    And Set as Default button is not visible for lastly added address


