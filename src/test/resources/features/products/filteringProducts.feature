@smoke @filter
Feature: Filtering search results

  Scenario Outline: products for scanners category are filtered by brand
    When left menu is opened on home page
    And Computers category is opened in left menu
    And Scanners subcategory is opened in left menu
    When filtering by '<brandName>' brand is selected
    Then should be displayed only products with '<brandName>'

    Examples:
      | brandName |
      | Fujitsu   |
      | SCHLAGE     |

  Scenario Outline: products for scanner category are filtered by rating
    Given search for '<searchString>' on home page
    When filtering by rating for '<countOfStarts>' and Up selected
    Then should be displayed only products with rating '<countOfStarts>' and Up

    Examples:
      | searchString           | countOfStarts  |
      | scanner canon formula | 4              |
      | scanner canon formula | 3              |

  Scenario: products are sorted by high to low price when sorting by high to low price is selected
    Given search for "Field Logic Big Shooter Crossbow Buck 3D" on home page
    When sorting by high to low price is selected
    Then products should be sorted by high to low price on all pages


