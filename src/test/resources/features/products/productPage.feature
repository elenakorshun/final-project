@smoke @productpage
Feature: Product Page is opened

  Scenario: Product page is opened for random product
    Given search for "nintendo switch" on home page
    When random product in search results is clicked on
    Then page with full information is displayed for selected product

