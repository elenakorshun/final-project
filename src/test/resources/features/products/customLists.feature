@regression @customlists
Feature: Custom Lists

  Background:
    Given user is logged in
    And shopping list is empty
    And search for "scanners" on home page
    And 1 -st product in search results is clicked on
    And add to list is pressed on product page for product

  Scenario: Product is added to shopping list
    Then confirmation message that product is added to shopping list should be shown
    And added product should be shown in shopping list

  Scenario: Product is deleted from shopping list
    Given shopping list page is opened
    When delete is pressed for product in shopping list
    Then section with product information is collapsed
    And count of products on refreshed page in list should be equal to 0

  Scenario: Shopping list view can be switched from list to grid
    Given shopping list page is opened
    When grid view is selected for shopping list
    Then products in shopping list should be displayed in grid mode

  Scenario: Share link to shopping list in view mode
    When shopping list page is opened
    And send list to others link is pressed on custom lists page
    And view only button is pressed in invite popup
    And copy link is pressed in invite popup
    And invite popup is closed on custom lists page
    And sign out button is pressed on home page
    And new browser window is opened with copied invite link to shopping list
    Then previous custom list should be opened
    And delete button for product in list should not be visible
    And move button for product in list should not be visible