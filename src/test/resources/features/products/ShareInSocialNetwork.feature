@smoke @shareinsocial
Feature: Share in Social network

  Scenario Outline: Social network page is opened when share option is selected for product
    Given search for "nintendo switch" on home page
    And random product in search results is clicked on
    When share in '<socialNetwork>' is selected for product
    And new tab is opened
    Then '<socialNetwork>' page should be opened

    Examples:
      | socialNetwork |
      | twitter       |
      | facebook      |
      | pinterest      |
      | facebook      |
