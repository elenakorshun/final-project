@smoke @cart
Feature: Cart

Background:
  Given search for "Brother DS-640 Compact Mobile Document Scanner, (Model: DS640)" on home page
  And 1 -st product in search results is clicked on
  And add to cart button is pressed on product page for product

  Scenario: Product is added to shopping cart
    Then confirmation that product is added to cart should be shown
    And cart contains 1 product and this product is the same as added

  Scenario Outline: Product quantity can be changed in the cart
    Given cart is opened
    When quantity for 1 -st product in cart is changed to '<quantity>'
    Then count of products in cart in main menu should be equal to '<quantity>'
    And count of products in cart at bottom Subtotal label should be equal to '<quantity>'
    And count of products in cart at right panel should be equal to '<quantity>'

    Examples:
      | quantity |
      | 2        |

  Scenario: Product is removed from shopping cart
    Given cart is opened
    When delete is pressed for 1 -st product in cart
    Then message that cart is empty should be shown
    Then count of products in cart in main menu should be equal to '0'

  Scenario: Help page for Gift Option is opened in new window
    Given cart is opened
    When learn more for product gift option is clicked on for 1 -st product in cart
    Then new page should be opened in new window
    Then opened page should contain information about gift option
