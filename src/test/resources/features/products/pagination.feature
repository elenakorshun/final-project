@regression @pagination
Feature: Pagination for Search Results

  Scenario: Pagination works correctly for Next button when search results are displayed on more than one page
    Given search for "Canon Canohh" on home page
    When Next page button is pressed until last page for search results is opened
    Then every opened page except first one should contain page number in url
    And highlighted page number on every opened page should be equal to current page
    And first product on every opened page should not be the same as on previous page