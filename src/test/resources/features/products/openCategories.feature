@smoke @filter
Feature: Categories for products are opened

  Scenario: Scanners product category is opened through left menu
    When left menu is opened on home page
    And Computers category is opened in left menu
    And Scanners subcategory is opened in left menu
    Then results are displayed for Scanners category

