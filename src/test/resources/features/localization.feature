@regression @localization
Feature: Localization for german language

  Scenario: Menu item Today's Deals is displayed in German language
    When german language is selected in search section on home page
    Then menu item Today's Deals is displayed as "Angebote des Tages"

