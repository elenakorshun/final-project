@regression @productvideo
Feature: Video for product can be stopped, played and liked

  Background:
    Given user is logged in
    And search for "nintendo switch" on home page
    And 1 -st product in search results is clicked on
    And video thumbnail is clicked on product page

  Scenario: video playback for product can be stopped
    When video playback is stopped
    Then video playback should be stopped

  Scenario: video playback for product can be played
    Given video playback is stopped
    When video playback is played
    Then video playback should be played

  Scenario: video playback for product can be liked
    When like icon is pressed for video
    Then like counter for video is increased to 1
    And like icon for video is highlighted

