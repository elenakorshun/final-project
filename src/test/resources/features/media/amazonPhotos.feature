@proxy
Feature: Amazon Photos

  Background:
    Given user is logged in
    And Your Account page is opened
    And Amazon Photos page is opened
    And photo list is empty
    And Add button on top panel is clicked on

  Scenario: photo can be uploaded to amazon photos
    When photo for uploading is selected using Upload photos option
    Then popup with loading process should appear
    And popup with successful uploading message should be displayed
    And uploaded photo should be displayed in list

  Scenario: photo can be deleted from amazon photos using drag&drop
    Given photo is uploaded to photos list
    When 1 -st photo in list is dragged&dropped to Move To Trash section
    And delete button is clicked in appeared popup
    Then list of photos should be empty

  Scenario: photo can be downloaded from amazon photos
    And photo is uploaded to photos list
    When 1 -st photo in list is clicked on
    And download option is selected in additional options dropdown
    Then downloaded photo should be the same as photo that was uploaded before

