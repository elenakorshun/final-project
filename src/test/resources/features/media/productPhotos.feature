@regression @productphoto
Feature: Photos for product are displaying

  Scenario: all photos for product are displayed in full view when cursor is hovered over them
    Given search for "Nintendo switch" on home page
    And 1 -st product in search results is clicked on
    When cursor is hovered over every available photo icon
    Then every photo is displayed in full view section