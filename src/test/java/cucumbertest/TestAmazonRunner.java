package cucumbertest;

import com.amazon.properties.PropertyReader;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import static com.amazon.properties.PropertyReader.PropertyName.*;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features", // можно специфические указать, а можно весь пакет
        glue = {"hooks" , "stepdefs"} // пакеты где ищутся степы, по умолчанию это test/java
)
public class TestAmazonRunner {

    @AfterClass
    public static void envProps()  {

        PropertyReader propertyReader = new PropertyReader();

        File allurePropsFile = new File(System.getProperty("user.dir")
                + "/target/allure-results/environment.properties");

        if (allurePropsFile.exists()) {
            allurePropsFile.delete();
        }

        try {
            Properties properties = new Properties();

            FileOutputStream fos = new FileOutputStream(allurePropsFile);

            properties.setProperty(homePageUrl.toString(), propertyReader.getPropertyValue(homePageUrl));
            properties.setProperty(shortWaitTime.toString(), propertyReader.getPropertyValue(shortWaitTime));
            properties.setProperty(longWaitTime.toString(), propertyReader.getPropertyValue(longWaitTime));
            properties.setProperty(superLongWaitTime.toString(), propertyReader.getPropertyValue(superLongWaitTime));
            properties.setProperty(pageLoadTimeout.toString(), propertyReader.getPropertyValue(pageLoadTimeout));
            properties.setProperty(directoryWithFilesToUpload.toString(), propertyReader.getPropertyValue(directoryWithFilesToUpload));
            properties.setProperty(directoryWithDownloadedFiles.toString(), propertyReader.getPropertyValue(directoryWithDownloadedFiles));
            properties.setProperty(fileNameToUpload.toString(), propertyReader.getPropertyValue(fileNameToUpload));
            properties.setProperty(directoryWithDownloadedFiles.toString(), propertyReader.getPropertyValue(directoryWithDownloadedFiles));
            properties.store(fos, "system props");
        }
       catch (IOException e){
            e.printStackTrace();
       }
    }

}


