package stepdefs;

import com.amazon.properties.PropertyReader;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.services.HomeService;
import com.amazon.utils.JSHelper;
import com.amazon.utils.CustomWaiter;

import static org.junit.Assert.assertTrue;

public class HomePageStepDefs {

    private HomeService homeService;
    private CustomWaiter customWaiter;
    private PropertyReader configFileReader;

    public HomePageStepDefs(HomeService homeService, CustomWaiter customWaiter, PropertyReader configFileReader){
        this.homeService = homeService;
        this.customWaiter = customWaiter;
        this.configFileReader = configFileReader;
    }

    @Then("home page should be opened")
    public void homePageShouldBeOpened() {
        assertTrue(homeService.waitUntilFirstGridBlockVisible());
    }

    @When("back To Top button at the bottom of home page is pressed")
    public void backToTopButtonAtTheBottomOfHomePageIsPressed() {
        homeService.moveToBackToTopBtn();
        homeService.pressBackToTopBtn();
    }

    @Then("home page is scrolled to the top")
    public void homePageIsScrolledToTheTop() {
        assertTrue(JSHelper.isElementInViewPortById(homeService.getLogoId()));
    }
}
