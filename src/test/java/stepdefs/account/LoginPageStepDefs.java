package stepdefs.account;

import com.amazon.properties.PropertyReader;
import com.amazon.utils.Logger;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.services.HomeService;
import com.amazon.services.accountservices.LoginService;
import com.amazon.states.LoginState;
import io.qameta.allure.Allure;


import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static com.amazon.properties.PropertyReader.PropertyName.*;

public class LoginPageStepDefs {

    private final LoginService loginService;
    private final HomeService homeService;
    private final PropertyReader propertyReader;
    private final LoginState loginState;

    public LoginPageStepDefs(LoginState loginState, LoginService loginService, HomeService homeService, PropertyReader configFileReader) {
        this.loginState = loginState;
        this.homeService = homeService;
        this.loginService = loginService;
        this.propertyReader = configFileReader;
    }

    @When("email is entered on login page")
    public void emailIsEntered() {
        loginService.enterEmail(propertyReader.getPropertyValue(userEmail));
    }

    @And("password is entered on login page")
    public void passwordIsEntered() {
        loginService.enterPassword(propertyReader.getPropertyValue(userPassword));
    }

    @And("user is logged in")
    public void userIsLoggedIn() {
        loginState.setGreetingMessageForGuest(homeService.getGreetingMessageText());
        homeService.clickOnAccountPanel();
        loginService.enterEmail(propertyReader.getPropertyValue(userEmail));
        loginService.enterPassword(propertyReader.getPropertyValue(userPassword));
    }

    @Then("login page should be opened")
    public void loginPageShouldBeOpened() {
        assertThat(url(), containsString(propertyReader.getPropertyValue(partOfSignInPageUrl)));
    }
}
