package stepdefs.account;

import com.amazon.blocks.productsblocks.AddressBlock;
import com.amazon.enums.CountryAbbreviation;
import io.cucumber.java.DataTableType;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.models.UserAddress;
import com.amazon.services.accountservices.AccountService;
import com.amazon.services.accountservices.UserAddressService;
import com.amazon.states.AddressState;
import com.amazon.utils.CustomWaiter;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class AccountStepDefs {

    private final AccountService accountService;
    private final UserAddressService userAddressService;
    private final AddressState addressState;

    public AccountStepDefs(AddressState addressState, AccountService accountService, UserAddressService userAddressService) {
        this.addressState = addressState;
        this.accountService = accountService;
        this.userAddressService = userAddressService;
    }

    @And("Your Account page is opened")
    public void yourAccountPageIsOpened() {
        accountService.clickOnAccountPanel();
    }

    @And("Your Addresses page is opened")
    public void yourAddressesPageIsOpened() {
        accountService.clickOnYourAddressesLink();
    }

    @DataTableType
    public UserAddress UserAddressEntry(Map<String, String> entry) {
        return new UserAddress(CountryAbbreviation.valueOf(entry.get("country")), entry.get("fullName"),
                entry.get("phoneNumber"), entry.get("addressLine1"), entry.get("addressLine2"),
                entry.get("city"), entry.get("state"), entry.get("zipCode"), Boolean.valueOf(entry.get("isDefaultAddress")));
    }

    @When("addresses are added:")
    public void addressesAreAdded(List<UserAddress> userAddresses) {
        if(userAddresses.size() > 1) {
            addressState.setFirstlyAddedAddressFullName(userAddresses.get(0).getFullName());
            addressState.setLastlyAddedAddressFullName(userAddresses.get(userAddresses.size() - 1).getFullName());
        }
        for (UserAddress ua : userAddresses) {
            userAddressService.clickOnAddAddressBtn();
            userAddressService.fillInUserAddress(ua);
            userAddressService.clickOnSaveBtn();
        }
    }

    @And("addresses list is empty except for residential address")
    public void addressesListIsEmptyExceptForResidentialAddress() {
        int addressesCount = userAddressService.getAddressesCountExceptResidential();
        for(int i = 0; i < addressesCount; i++){
            userAddressService.deleteAddressByIndex(0); // deleting first address in list
        }
    }

    @Then("{int} address/addresses is/are appeared in address list\\(except for residential address)")
    public void addressIsAppearedInAddressListExceptForResidentialAddress(int addressCount) {
        assertEquals(addressCount, userAddressService.getAddressesCountExceptResidential());
    }

    @Then("Set as Default button is visible for firstly added address")
    public void setAsDefaultButtonIsVisibleForFirstlyAddedAddress() {
        AddressBlock addressBlock = userAddressService.getAddressBlockByFullName(addressState.getFirstlyAddedAddressFullName());
        assertTrue(addressBlock.getSetAsDefaultLink().isDisplayed());
    }

    @And("Set as Default button is not visible for lastly added address")
    public void setAsDefaultButtonIsNotVisibleForLastlyAddedAddress() {
        AddressBlock addressBlock = userAddressService.getAddressBlockByFullName(addressState.getLastlyAddedAddressFullName());
        assertFalse(addressBlock.getSetAsDefaultLink().isDisplayed());
    }
}
