package stepdefs;

import com.amazon.utils.Logger;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.services.HomeService;
import com.amazon.services.accountservices.LoginService;
import com.amazon.states.LoginState;
import com.amazon.states.SearchResultsState;
import com.amazon.utils.CustomWaiter;

import static com.codeborne.selenide.Selenide.sleep;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class HeaderMenuStepDefs {

    private HomeService homeService;
    private LoginService loginService;

    private final LoginState loginState;
    private final SearchResultsState searchResultsState;
    private CustomWaiter customWaiter;

    public HeaderMenuStepDefs(LoginState loginState, SearchResultsState searchResultsState,
                              HomeService homeService, LoginService loginService, CustomWaiter customWaiter) {
        this.loginState = loginState;
        this.searchResultsState = searchResultsState;
        this.homeService = homeService;
        this.loginService = loginService;
        this.customWaiter= customWaiter;
    }

    @When("german language is selected in search section on home page")
    public void germanLanguageIsSelectedInSearchSectionOnHomePage() {
        homeService.hoverOverLanguagePanel();
        homeService.pressGermanLanguageLink();
    }

    @Then("menu item Today's Deals is displayed as {string}")
    public void menuItemTodaysDealsIsDisplayedAs(String menuItemText) {
        assertEquals(homeService.getTodaysDealsMenuLinkText(), menuItemText);
    }

    @Then("sign in button should not be visible on home page")
    public void signInButtonShouldNotBeVisible() {
        homeService.hoverOverAccountPanel();
        assertFalse(homeService.waitUntilSignInBtnVisible());
    }

    @And("sign out button should be visible on home page")
    public void signOutButtonShouldBeVisible() {
        homeService.hoverOverAccountPanel();
        assertTrue(homeService.waitUntilSignOutBtnVisible());
    }

    @When("sign out button is pressed on home page")
    public void signOutButtonIsPressed() {
        homeService.hoverOverAccountPanel();
        homeService.pressSignOutBtn();
        Logger.info("User is signed out");
    }

    @And("default greeting message for guest should not be displayed on home page")
    public void defaultGreetingMessageForGuestShouldNotBeDisplayedOnHomePage() {
        assertThat(homeService.getGreetingMessageText(), not(containsString(loginState.getGreetingMessageForGuest())));
    }

    @And("login page is opened")
    public void loginPageIsOpened() {
        loginState.setGreetingMessageForGuest(homeService.getGreetingMessageText());
        Logger.debug("greeting message is saved to state");
        homeService.hoverOverAccountPanel();
        homeService.pressSignInBtn();
        Logger.info("Login page is opened");
    }


    @And("default greeting message for guest should be displayed on home page")
    public void defaultGreetingMessageForGuestShouldBeDisplayed() {
        loginService.returnToHomePage();
        assertThat(homeService.getGreetingMessageText(), containsString(loginState.getGreetingMessageForGuest()));
    }

    @When("left menu is opened on home page")
    public void leftMenuIsOpenedOnHomePage() {
        homeService.pressAllMenuLink();
    }

    @And("Computers category is opened in left menu")
    public void computersCategoryIsOpenedInLeftMenu() {
        homeService.pressComputersLink();
    }

    @And("Scanners subcategory is opened in left menu")
    public void scannersSubcategoryIsOpenedInLeftMenu() {
        searchResultsState.setSubCategoryText(homeService.getScannersLinkText());
        homeService.pressScannersLink();
    }

    @And("search for {string} on home page")
    public void searchForOnHomePage(String searchString) {
        homeService.enterTextToSearchBar(searchString);
        homeService.pressSearchBtn();
    }

    @And("cart is opened")
    public void cartIsOpened() {
        homeService.pressCartLink();
    }
}
