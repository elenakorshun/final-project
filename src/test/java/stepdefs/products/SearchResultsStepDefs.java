package stepdefs.products;

import com.amazon.blocks.productsblocks.ProductBlock;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.models.PageData;
import org.assertj.core.api.SoftAssertions;
import com.amazon.services.productservices.SearchResultsService;
import com.amazon.states.ProductState;
import com.amazon.states.SearchResultsState;

import java.util.*;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class SearchResultsStepDefs {

    private final SearchResultsService searchResultsService;
    private final SearchResultsState searchResultsState;
    private final ProductState productState;

    public SearchResultsStepDefs(SearchResultsState searchResultsState, ProductState productState,
                                 SearchResultsService searchResultsService) {
        this.searchResultsState = searchResultsState;
        this.productState = productState;
        this.searchResultsService = searchResultsService;
    }

    @Then("results are displayed for Scanners category")
    public void resultsAreDisplayedForScannersCategory() {
        assertEquals(searchResultsService.getDepartmentSubCategoryText(), searchResultsState.getSubCategoryText());
    }

    @And("random product in search results is clicked on")
    public void randomProductInSearchResultsIsClickedOn() {
        int randomProductIndex =  new Random().nextInt(searchResultsService.getCountOfProductsCurrentPage());
        ProductBlock randomProductBlock = searchResultsService.getProductBlockByIndex(randomProductIndex);
        productState.setProductTitle(randomProductBlock.getProductTitle());
        randomProductBlock.clickOnProduct();
    }


    @When("Next page button is pressed until last page for search results is opened")
    public void nextPageButtonIsPressedUntilLastPageForSearchResultsIsOpened() {
        searchResultsState.setPageDataList(searchResultsService.moveThroughAllAvailablePagesUsingNext());
    }


    @When("sorting by high to low price is selected")
    public void sortingByHighToLowPriceIsSelected() {
        searchResultsService.clickOnSortingDropDown();
        searchResultsService.selectPriceDescSorting();
    }

    @Then("products should be sorted by high to low price on all pages")
    public void productsAreSortedByHighToLowPriceOnAllPages() {
        List<Double> productPriceOnAllPagesList = searchResultsService.getProductsPriceOnAllPages();
        List<Double> copy = new ArrayList<>(productPriceOnAllPagesList);
        copy.sort(Collections.reverseOrder());
        assertEquals(copy, productPriceOnAllPagesList);
    }

    @When("filtering by {string} brand is selected")
    public void filteringByBrandNameBrandIsSelected(String brand) {
        searchResultsService.checkBrandCheckboxFor(brand);
    }

    @Then("should be displayed only products with {string}")
    public void shouldBeDisplayedOnlyProductsWithBrandName(String brand) {
        List<String> productTitlesOnAllPagesList = searchResultsService.getProductsTitleOnAllPagesList();
        assertThat(productTitlesOnAllPagesList, everyItem(containsStringIgnoringCase(brand)));
    }

    @Then("every opened page except first one should contain page number in url")
    public void everyOpenedPageExceptFirstOneShouldContainPageNumberInUrl() {
        List<String> pageUrls = searchResultsState.getPageDataList().stream()
                .map(PageData::getPageUrl)
                .collect(Collectors.toList());
        int startPageForCheck = 2; // start with page 2
        pageUrls.remove(0); // start with page 2
        SoftAssertions softAssertions = new SoftAssertions();
        String expectedPageUrl;
        for(String url : pageUrls) {
            expectedPageUrl = String.format("page=%d", startPageForCheck);
            softAssertions.assertThat(url).contains(expectedPageUrl);
            startPageForCheck++;
        }
        softAssertions.assertAll();
    }

    @And("highlighted page number on every opened page should be equal to current page")
    public void highlightedPageNumberOnEveryOpenedPageShouldBeEqualToCurrentPage() {
        List<String> pageNumbers = searchResultsState.getPageDataList().stream()
                .map(PageData::getPageNumber)
                .collect(Collectors.toList());
        SoftAssertions softAssertions = new SoftAssertions();
        int expectedNumber = 1;
        for(String pageNumber : pageNumbers) {
            softAssertions.assertThat(Integer.valueOf(pageNumber)).isEqualTo(expectedNumber);
            expectedNumber++;
        }
        softAssertions.assertAll();
    }

    @And("first product on every opened page should not be the same as on previous page")
    public void firstProductOnEveryOpenedPageShouldNotBeTheSameAsOnPreviousPage() {
        List<String> productTitles = searchResultsState.getPageDataList().stream()
                .map(PageData::getFirstProductTitle)
                .collect(Collectors.toList());
        assertEquals(new HashSet<String>(productTitles).size(), productTitles.size());
    }

    @And("{int} -st/nd/rd/th product in search results is clicked on")
    public void stProductInSearchResultsIsClickedOn(int productIndex) {
        productState.setProductTitle(searchResultsService.getProductBlockByIndex(productIndex - 1).getProductTitle());
        searchResultsService.getProductBlockByIndex(productIndex - 1).clickOnProduct();
    }

    @When("filtering by rating for {string} and Up selected")
    public void filteringByRatingForCountOfStartsAndUpSelected(String rating) {
        searchResultsService.clickOnRatingFilterFor(rating);
    }

    @Then("should be displayed only products with rating {string} and Up")
    public void shouldBeDisplayedOnlyProductsWithRatingCountOfStartsAndUp(String rating) {
        assertThat(searchResultsService.getProductsRatingOnAllPagesList(),
                everyItem(greaterThanOrEqualTo(Double.parseDouble(rating))));
    }
}
