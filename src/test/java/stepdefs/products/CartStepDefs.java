package stepdefs.products;

import com.amazon.properties.PropertyReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import com.amazon.services.productservices.CartService;
import com.amazon.services.productservices.GiftOptionService;
import com.amazon.services.HomeService;
import com.amazon.states.BrowserState;
import com.amazon.states.ProductState;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.*;

public class CartStepDefs {

    private final CartService cartService;
    private final HomeService homeService;
    private final GiftOptionService giftOptionService;
    private final ProductState productState;
    private final BrowserState browserState;
    private final PropertyReader configFileReader;

    public CartStepDefs(ProductState productState, BrowserState browserState, CartService cartService,
                        HomeService homeService, GiftOptionService giftOptionService,
                        PropertyReader configFileReader) {
        this.productState = productState;
        this.browserState = browserState;
        this.cartService = cartService;
        this.homeService = homeService;
        this.giftOptionService = giftOptionService;
        this.configFileReader = configFileReader;
    }

    @Then("count of products in cart in main menu should be equal to {string}")
    public void countOfProductsInCartInHeaderMenuShouldBeEqualToQuantity(String totalItems) {
        assertTrue(homeService.waitUntilCartCountLblHasText(totalItems));
    }

    @Then("count of products in cart at bottom Subtotal label should be equal to {string}")
    public void countOfProductsInCartAtBottomSubtotalLabelShouldBeEqualToQuantity(String totalItems) {
        assertTrue(cartService.waitUntilBottomSubtotalLblHasText(totalItems));
    }

    @Then("count of products in cart at right panel should be equal to {string}")
    public void countOfProductsInCartAtRightPanelShouldBeEqualToQuantity(String totalItems) {
        assertTrue(cartService.waitUntilProceedToCheckOutSubtotalLblHasText(totalItems));
    }

    @Then("message that cart is empty should be shown")
    public void messageThatCartIsEmptyShouldBeShown() {
        assertTrue(cartService.waitUntilCartIsEmptyLblVisible());
    }

    @Then("opened page should contain information about gift option")
    public void openedPageShouldContainInformationAboutGiftOption() {
        switchTo().window(1);
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(giftOptionService.waitUntilHelpContentSectionVisible()).isTrue();
        softAssertions.assertThat(url()).
                contains(configFileReader.getPropertyValue(PropertyReader.PropertyName.partOfGiftOptionHelpPageUrl));
        softAssertions.assertAll();
    }

    @When("delete is pressed for {int} -st/nd/rd/th product in cart")
    public void deleteIsPressedForStInCart(int productIndex) {
        cartService.getCartProductBlockByIndex(productIndex - 1).ClickOnDeleteBtn();
    }

    @When("quantity for {int} -st/nd/rd/th product in cart is changed to {string}")
    public void quantityForStProductInCartIsChangedToQuantity(int productIndex, String quantity) {
        cartService.getCartProductBlockByIndex(productIndex - 1).setProductQuantity(quantity);
    }

    @When("learn more for product gift option is clicked on for {int} -st/nd/rd/th product in cart")
    public void learnMoreForProductGiftOptionIsClickedOnForStProductInCart(int productIndex) {
        browserState.setCountOfOpenedWindows(getWebDriver().getWindowHandles().size());
        cartService.getCartProductBlockByIndex(productIndex - 1).ClickOnLearnMoreLink();
    }

    @And("cart contains {int} product and this product is the same as added")
    public void cartContainsProductAndThisProductIsTheSameAsAdded(int productCount) {
        homeService.pressCartLink();
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(cartService.getCountOfCartProducts()).isEqualTo(productCount);
        softAssertions.assertThat(cartService.getCartProductBlockByIndex(0).getCartProductTitleText()).isEqualTo(productState.getProductTitle());
        softAssertions.assertAll();
    }
}
