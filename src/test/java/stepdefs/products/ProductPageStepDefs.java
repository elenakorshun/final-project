package stepdefs.products;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.SoftAssertions;
import com.amazon.services.productservices.ProductService;
import com.amazon.states.ProductState;
import com.amazon.states.SearchResultsState;
import com.amazon.utils.CustomWaiter;
import com.amazon.utils.ScreenShotHelper;

import java.awt.image.BufferedImage;
import java.util.List;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.url;
import static org.junit.Assert.*;

public class ProductPageStepDefs {

    private final ProductService productService;
    private final SearchResultsState searchResultsState;
    private final ProductState productState;
    private final CustomWaiter customWaiter;

    public ProductPageStepDefs(SearchResultsState searchResultsState, ProductState productState, ProductService productService, CustomWaiter customWaiter) {
        this.searchResultsState = searchResultsState;
        this.productState = productState;
        this.productService = productService;
        this.customWaiter = customWaiter;
    }

    @When("share in {string} is selected for product")
    public void shareInSocialNetworkIsSelectedForProduct(String socialNetwork) {
        productService.clickOnShareInSocialNetwork(socialNetwork);
    }

    @Then("{string} page should be opened")
    public void socialNetworkPageShouldBeOpened(String socialNetwork) {
        assertTrue(url().contains(socialNetwork + ".com"));
    }

    @When("add to cart button is pressed on product page for product")
    public void addToCartButtonIsPressedOnPageProductPage() {
        productState.setProductTitle(productService.getProductTitle());
        productService.clickOnAddToCartBtn();
    }

    @Then("confirmation that product is added to cart should be shown")
    public void confirmationThatProductIsAddedToCartShouldBeShown() {
        assertTrue(productService.waitUntilConfirmTextIsVisible());
    }

    @Then("page with full information is displayed for selected product")
    public void pageWithFullInformationIsDisplayedForSelectedProduct() {
        assertEquals(productState.getProductTitle(), productService.getProductTitle());
    }

    @When("add to list is pressed on product page for product")
    public void addToListIsPressedOnProductPageForProduct() {
        productService.clickOnAddToListBtn();
    }

    @Then("confirmation message that product is added to shopping list should be shown")
    public void confirmationMessageThatProductIsAddedToShoppingListShouldBeShown() {
        assertTrue(productService.waitUntilConfirmTextForAddToListIsVisible());
    }

    @And("shopping list page is opened")
    public void shoppingListPageIsOpened() {
        productService.clickOnViewYourListBtn();
    }

    @When("cursor is hovered over every available photo icon")
    public void cursorIsHoveredOverEveryAvailablePhotoIcon() {
        productState.setProductPhotos(productService.getScreenShotsForEveryPhoto());
    }

    @Then("every photo is displayed in full view section")
    public void everyPhotoIsDisplayedInFullViewSection() {
        SoftAssertions softAssertions = new SoftAssertions();
        ScreenShotHelper screenShotHelper = new ScreenShotHelper();
        List<BufferedImage> screenshotsForEveryPhoto = productState.getProductPhotos();
        BufferedImage previousScreenShot = null;
        for(BufferedImage currentScreenshot : screenshotsForEveryPhoto){
            if(previousScreenShot != null) {
                softAssertions.assertThat(screenShotHelper.isImagesDifferent(previousScreenShot, currentScreenshot)).isTrue();
            }
            previousScreenShot = currentScreenshot;
        }
        softAssertions.assertAll();
    }
}
