package stepdefs.products;

import com.codeborne.selenide.Selenide;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.services.productservices.CustomListService;
import com.amazon.services.productservices.ProductService;
import com.amazon.states.ProductState;
import com.amazon.utils.CustomWaiter;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.*;
import static org.junit.Assert.*;

public class CustomListStepDefs {

    private final CustomListService customListService;
    private final ProductService productService;
    private final ProductState productState;
    private final CustomWaiter customWaiter;

    public CustomListStepDefs(ProductState productState, ProductService productService, CustomListService customListService, CustomWaiter customWaiter) {
        this.productState = productState;
        this.customListService = customListService;
        this.productService = productService;
        this.customWaiter = customWaiter;
    }

    @And("added product should be shown in shopping list")
    public void addedProductShouldBeShownInShoppingList() {
        productService.clickOnViewYourListBtn();
        assertEquals(productState.getProductTitle(),
                customListService.getCustomListProductBlockByIndex(0).getCustomListProductTitle());
    }

    @When("delete is pressed for product in shopping list")
    public void deleteIsPressedForProductInShoppingList() {
        customListService.getCustomListProductBlockByIndex(0).clickOnDeleteProductFromListBtn();
    }

    @Then("section with product information is collapsed")
    public void sectionWithProductInformationIsCollapsed() {
        assertTrue(customListService.getCustomListProductBlockByIndex(0).getProductImg().isDisplayed());
    }

    @And("count of products on refreshed page in list should be equal to {int}")
    public void countOfProductsOnRefreshedPageInListShouldBeEqualTo(int expectedCount) {
        Selenide.refresh();
        assertEquals(expectedCount, customListService.getCustomListProductBlockCount());
    }

    @When("grid view is selected for shopping list")
    public void gridViewIsSelectedForShoppingList() {
        customListService.clickOnGridViewSwitcher();
    }

    @Then("products in shopping list should be displayed in grid mode")
    public void productsInShoppingListShouldBeDisplayedInGridMode() {
        assertTrue(customListService.waitUntilProductGridContainerHasGridView());
    }

    @When("send list to others link is pressed on custom lists page")
    public void sendListToOthersLinkIsPressedOnCustomListsPage() {
        productState.setProductTitle(customListService.getCustomListProductBlockByIndex(0).getCustomListProductTitle());
        customListService.clickOnShareListToOthersLink();
    }

    @And("view only button is pressed in invite popup")
    public void viewOnlyButtonIsPressedInInvitePopup() {
        customListService.clickOnViewOnlyBtn();
    }

    @And("copy link is pressed in invite popup")
    public void copyLinkIsPressedInInvitePopup() {
        customListService.clickOnCopyLinkForViewOnlyLink();
    }

    @And("invite popup is closed on custom lists page")
    public void invitePopupIsClosedOnCustomListsPage() {
       customListService.clickOnCloseInvitePopUpBtn();
        sleep(500); // sing out link isn't clicked right after popup is closed
    }

    @Then("previous custom list should be opened")
    public void previousCustomListShouldBeOpened() {
        assertEquals(productState.getProductTitle(), customListService.getCustomListProductBlockByIndex(0).getCustomListProductTitle());
    }

    @And("delete button for product in list should not be visible")
    public void deleteButtonForProductInListShouldNotBeVisible() {
        assertFalse(customListService.getCustomListProductBlockByIndex(0).getDeleteProductFromListBtn().isDisplayed());
    }

    @And("move button for product in list should not be visible")
    public void moveButtonForProductInListShouldNotBeVisible() {
        assertFalse(customListService.getCustomListProductBlockByIndex(0).getMoveProductFromListBtn().isDisplayed());
    }

    @And("shopping list is empty")
    public void shoppingListIsEmpty() {
        customListService.hoverOverAccountPanel();
        customListService.clickOnShoppingListLinkBtn();
        customListService.clickOnDeleteButtonForAllItemsFromShoppingList();
    }

}
