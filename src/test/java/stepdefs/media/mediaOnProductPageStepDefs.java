package stepdefs.media;

import com.amazon.properties.PropertyReader;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import com.amazon.services.productservices.ProductService;
import com.amazon.states.ProductState;
import com.amazon.utils.ScreenShotHelper;

import java.awt.image.BufferedImage;

import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.*;
import static com.amazon.properties.PropertyReader.PropertyName.*;

public class mediaOnProductPageStepDefs {

    private final ProductService productService;
    private final ProductState productState;
    private final PropertyReader propertyReader;

    public mediaOnProductPageStepDefs(ProductState productState, ProductService productService, PropertyReader propertyReader) {
        this.productState = productState;
        this.productService = productService;
        this.propertyReader = propertyReader;
    }

    @And("video thumbnail is clicked on product page")
    public void videoThumbnailIsClickedOnProductPage() {
        productService.clickOnVideoThumbnail();
    }

    @When("like icon is pressed for video")
    public void likeIconIsPressedForVideo() {
        productState.setVideoLikeCounter(productService.getVideoLikeCount());
        productService.clickOnVideoLikeIcon();
    }

    @When("video playback is stopped")
    public void videoPlaybackIsStopped() {
        sleep(4000); // give video some time to play before stopping it
        productService.stopVideo();
    }

    @When("video playback is played")
    public void videoPlaybackIsPlayed() {
        productService.runVideo();
    }

    @Then("video playback should be stopped")
    public void videoPlaybackShouldBeStopped() {
        ScreenShotHelper screenShotHelper = new ScreenShotHelper();
        BufferedImage stoppedVideoScreenshot = productService.getScreeenshotOfVideoPlayer();
        sleep(Long.parseLong(propertyReader.getPropertyValue(shortWaitTime))); // wait to make sure video is stopped before making screenshot
        BufferedImage stoppedVideoScreenshotLater = productService.getScreeenshotOfVideoPlayer();
        assertFalse(screenShotHelper.isImagesDifferent(stoppedVideoScreenshot, stoppedVideoScreenshotLater));
    }

    @Then("video playback should be played")
    public void videoPlaybackShouldBePlayed() {
        ScreenShotHelper screenShotHelper = new ScreenShotHelper();
        BufferedImage playingVideoScreenshot = productService.getScreeenshotOfVideoPlayer();
        sleep(Long.parseLong(propertyReader.getPropertyValue(shortWaitTime))); // wait to make sure video is playing before making screenshot
        BufferedImage playingVideoScreenshotLater = productService.getScreeenshotOfVideoPlayer();
        assertTrue(screenShotHelper.isImagesDifferent(playingVideoScreenshot, playingVideoScreenshotLater));
    }

    @Then("like counter for video is increased to {int}")
    public void likeCounterForVideoIsIncreasedTo(int increaseStep) {
        assertEquals(productState.getVideoLikeCounter() + increaseStep, productService.getVideoLikeCount());
    }

    @And("like icon for video is highlighted")
    public void likeIconForVideoIsHighlighted() {
        assertTrue(productService.getVideoLikeIconClassValue().contains("show"));
        productService.clickOnVideoLikeIcon(); // return to not liked state, need to rework

    }
}
