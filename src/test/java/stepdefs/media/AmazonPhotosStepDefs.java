package stepdefs.media;

import com.amazon.properties.PropertyReader;
import com.amazon.services.AmazonPhotosService;
import com.amazon.services.accountservices.AccountService;
import com.amazon.utils.CustomWaiter;
import com.amazon.utils.FileHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.File;

import static com.amazon.properties.PropertyReader.PropertyName.*;
import static com.codeborne.selenide.Selenide.refresh;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.assertTrue;

public class AmazonPhotosStepDefs {

    private final AmazonPhotosService amazonPhotosService;
    private final AccountService accountService;
    private final PropertyReader propertyReader;

    private final String PROJECT_DIR = System.getProperty("user.dir");

    public AmazonPhotosStepDefs(AmazonPhotosService amazonPhotosService,
                                AccountService accountService, PropertyReader propertyReader) {
        this.amazonPhotosService = amazonPhotosService;
        this.accountService = accountService;
        this.propertyReader = propertyReader;
    }

    @And("Amazon Photos page is opened")
    public void amazonPhotosPageIsOpened() {
        accountService.clickOnManageDriveAndPhotosLink();
        amazonPhotosService.clickOnBackBtn();
    }

    @When("Add button on top panel is clicked on")
    public void addButtonOnTopPanelIsClickedOn() {
        amazonPhotosService.clickOnAddPhotoBtn();
    }

    @And("photo for uploading is selected using Upload photos option")
    public void photoForUploadingIsSelectedUsingUploadPhotosOption() {
        File fileToUpload = new File(PROJECT_DIR
                + propertyReader.getPropertyValue(PropertyReader.PropertyName.directoryWithFilesToUpload)
                +  propertyReader.getPropertyValue(fileNameToUpload));
        amazonPhotosService.uploadPhoto(fileToUpload);
    }

    @And("popup with successful uploading message should be displayed")
    public void popupWithSuccessfulUploadingMessageShouldBeDisplayed() {
        assertTrue(amazonPhotosService.waitUntilSuccessUploadingPopUpIsVisible());
    }

    @And("download option is selected in additional options dropdown")
    public void downloadOptionIsSelectedInAdditionalOptionsDropdown() {
        amazonPhotosService.clickOnExpandableOptionsBtn();
        amazonPhotosService.downloadPhoto();
        sleep(Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime))); // wait until file is downloaded
    }

    @When("{int} -st/nd/rd/th photo in list is clicked on")
    public void stPhotoInListIsClickedOn(int photoOrderInList) {
        amazonPhotosService.clickOnPhotoByIndex(photoOrderInList - 1);
    }

    @And("photo is uploaded to photos list")
    public void photoIsUploadedToPhotosList() {
        File fileToUpload = new File(PROJECT_DIR
                + propertyReader.getPropertyValue(directoryWithFilesToUpload)
                + propertyReader.getPropertyValue(fileNameToUpload));
        amazonPhotosService.uploadPhoto(fileToUpload);
        amazonPhotosService.waitUntilPhotoIsUploaded();
    }

    @Then("popup with loading process should appear")
    public void popupWithLoadingProcessShouldAppear() {
        assertTrue(amazonPhotosService.waitUntilUploadingPopUpIsVisible());
    }

    @Then("downloaded photo should be the same as photo that was uploaded before")
    public void downloadedPhotoShouldBeTheSameAsPhotoThatWasUploadedBefore() {
        FileHelper fileHelper = new FileHelper();
        assertTrue(fileHelper.isFilePresentInDirectory(propertyReader.getPropertyValue(fileNameToUpload),
                PROJECT_DIR + propertyReader.getPropertyValue(directoryWithDownloadedFiles)));
    }

    @When("{int} -st/nd/rd/th photo in list is dragged&dropped to Move To Trash section")
    public void stPhotoInListIsDraggedDroppedToMoveToTrashSection(int photoOrderInList) {
        amazonPhotosService.dragPhotoByIndex(photoOrderInList - 1);
        amazonPhotosService.clickOnTrashBinToDoSection();
    }

    @And("delete button is clicked in appeared popup")
    public void deleteButtonIsClickedInAppearedPopup() {
        amazonPhotosService.clickOnConfirmDeleteBtn();
    }

    @Then("list of photos should be empty")
    public void listOfPhotosShouldBeEmpty() {
        assertTrue(amazonPhotosService.waitUntilEmptyStateElementIsVisible());
    }

    @And("photo list is empty")
    public void photoListIsEmpty() {
        if(amazonPhotosService.waitForFirstPhotoIsDisplayed()){
            int photosCount = amazonPhotosService.getPhotosCollectionSize();
            while (photosCount-- != 0) {
                amazonPhotosService.dragPhotoByIndex(0); // first photo in list
                amazonPhotosService.clickOnTrashBinToDoSection();
                amazonPhotosService.clickOnConfirmDeleteBtn();
                amazonPhotosService.waitUntilDeleteSuccessPopUpVisible();
            }
        }
    }

    @And("uploaded photo should be displayed in list")
    public void uploadedPhotoShouldBeDisplayedInList() {
        assertTrue(amazonPhotosService.waitUntilFirstPhotoIsVisible());
    }
}
