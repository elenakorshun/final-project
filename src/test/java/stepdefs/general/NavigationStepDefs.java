package stepdefs.general;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import com.amazon.states.BrowserState;
import com.amazon.utils.ClipBoardHelper;

import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class NavigationStepDefs {

    private final BrowserState browserState;

    public NavigationStepDefs(BrowserState browserState) {
        this.browserState = browserState;
    }

    @And("new tab is opened")
    public void newTabIsOpened() {
        switchTo().window(1);
    }

    @Then("new page should be opened in new window")
    public void newPageShouldBeOpenedInNewWindow() {
        assertEquals(browserState.getCountOfOpenedWindows() + 1, getWebDriver().getWindowHandles().size());
    }

    @And("new browser window is opened with copied invite link to shopping list")
    public void newBrowserWindowIsOpenedWithCopiedInviteLinkToShoppingList() {
        ClipBoardHelper clipBoardHelper = new ClipBoardHelper();
        open(clipBoardHelper.getCopiedValueFromClipBoard());
    }
}
