package hooks;

import com.amazon.properties.PropertyReader;
import com.amazon.utils.FileHelper;
import com.amazon.utils.Logger;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import io.cucumber.java.After;
import io.cucumber.java.Before;

import static com.amazon.properties.PropertyReader.PropertyName.*;
import static com.codeborne.selenide.FileDownloadMode.PROXY;
import static com.codeborne.selenide.Selenide.open;

public class Hooks {

    private final String PROJECT_DIR = System.getProperty("user.dir");
    PropertyReader propertyReader = new PropertyReader();

    @Before(value = "not @proxy", order = 10)
    public void beforeScenarioSeleniumGridTurnOn() {
       // Configuration.remote = "http://localhost:4444/wd/hub";
        Logger.debug("Scenario is started on selenide node");
    }

    @Before(order = 20)
    public void beforeScenario() {
        Configuration.pageLoadTimeout = Long.parseLong(propertyReader.getPropertyValue(pageLoadTimeout));
        open(propertyReader.getPropertyValue(homePageUrl));
        Logger.info("Home Page is opened");
        Logger.debug("WebDriver is started");
    }

    @After
    public void afterScenario(){
        WebDriverRunner.closeWebDriver();
        Logger.info("Browser is closed");
        Logger.debug("WebDriver is closed");
    }

    @Before(value = "@proxy", order = 0)
    public void turnOnProxy(){
        Configuration.proxyEnabled = true;
        Configuration.fileDownload = PROXY;
        Logger.debug("Proxy is turn on");
    }

    @After("@proxy")
    public void deleteDirectoryWithTestFiles() {
        FileHelper fileHelper = new FileHelper();
        fileHelper.deleteDirectory(PROJECT_DIR + propertyReader.getPropertyValue(directoryWithDownloadedFiles));
        Logger.debug("Directory with downloaded files is deleted");
    }
}