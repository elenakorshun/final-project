package com.amazon.pages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class HomePage {

    private final SelenideElement firstGridBlock = $(By.id("desktop-grid-1"));

    private final SelenideElement BackToTopBtn = $(By.className("navFooterBackToTop"));

    private final String mainLogoId = "nav-logo-sprites";

}
