package com.amazon.pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class AmazonPhotosPage {

    private final SelenideElement addPhotoBtn = $(By.xpath("//div[contains(@class,'add-button')]/button"));

    private final SelenideElement backBtn = $(By.className("back"));

    private final SelenideElement uploadingPopUp = $(By.className("uploader-status"));

    private final SelenideElement successUploadingPopUp = $(By.xpath("//section[@class='uploader-complete succeeded']"));

    private final ElementsCollection photosCollection = $$(By.className("mosaic-item"));

    private final SelenideElement closeUploadPopupBtn = $(By.xpath("//section[contains(@class,'uploader-complete succeeded')]//button[@class='close']"));

    private final SelenideElement expandableOptionsBtn = $(By.xpath("//div[@class='expandable-nav more']"));

    private final SelenideElement uploadButton =  $(By.xpath("//input[@type='file']"));

    private final SelenideElement downloadBtn =  $(By.xpath("//button[@class='download']"));

    private final SelenideElement PhotosCountLbl = $(By.className("count"));

    private final SelenideElement imageHeadOptionsPanel = $(By.className("image-head"));

    private final SelenideElement trashBinToDoSection = $(By.name("trash"));

    private final SelenideElement trashBinHeaderIcon = $(By.xpath("//button[@class='trash']"));

    private final SelenideElement ConfirmDeleteBtn = $(By.xpath("//div[@class='options']//button[@class='button']"));

    private final SelenideElement emptyStateElement = $(By.className("empty-state-content"));

    private final SelenideElement deleteSuccessPopUp =  $(By.name("DeleteToastSuccess"));

    private final SelenideElement firstPhoto = $(By.className("mosaic-item"));
}
