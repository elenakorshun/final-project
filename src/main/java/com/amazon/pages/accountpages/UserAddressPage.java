package com.amazon.pages.accountpages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class UserAddressPage {

    private final SelenideElement addAddressBtn = $(By.id("ya-myab-plus-address-icon"));

    private final SelenideElement fullNameTextBox = $(By.id("address-ui-widgets-enterAddressFullName"));

    private final SelenideElement phoneNumberTextBox = $(By.id("address-ui-widgets-enterAddressPhoneNumber"));

    private final SelenideElement cityTextBox = $(By.id("address-ui-widgets-enterAddressCity"));

    private final SelenideElement addressLine1TextBox = $(By.id("address-ui-widgets-enterAddressLine1"));

    private final SelenideElement addressLine2TextBox = $(By.id("address-ui-widgets-enterAddressLine2"));

    private final SelenideElement countrySelect = $(By.id("address-ui-widgets-countryCode-dropdown-nativeId"));

    private final SelenideElement stateSelect = $(By.id("address-ui-widgets-enterAddressStateOrRegion-dropdown-nativeId"));

    private final SelenideElement defaultAddressCheckbox = $(By.id("address-ui-widgets-use-as-my-default"));

    private final SelenideElement postalCodeTextBox = $(By.id("address-ui-widgets-enterAddressPostalCode"));

    private final SelenideElement saveBtn = $(By.id("address-ui-widgets-form-submit-button"));

    private final ElementsCollection addresses = $$(By.xpath("//div[contains(@id,'ya-myab-display-address-block-')]/.."));

    private final SelenideElement deletePopUp = $(By.xpath("//div[contains(@class,'a-popover a-popover-modal a-declarative')]"));

    public final SelenideElement getConfirmRemovalForAddressByIndexBtn(int index) {
        return $(By.id(String.format("deleteAddressModal-%d-submit-btn", index)));
    }

}
