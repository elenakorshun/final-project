package com.amazon.pages.accountpages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class LoginPage {

    private final SelenideElement emailInput = $(By.id("ap_email"));

    private final SelenideElement emailContinueBtn = $(By.id("continue"));

    private final SelenideElement passwordInput = $(By.id("ap_password"));

    private final SelenideElement signInBtn = $(By.id("signInSubmit"));

    private final SelenideElement amazonLbl = $(By.xpath("//i[@aria-label='Amazon']"));


}
