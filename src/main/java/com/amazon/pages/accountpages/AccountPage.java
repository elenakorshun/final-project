package com.amazon.pages.accountpages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class AccountPage {

    private final SelenideElement manageDriveAndPhotosLink = $(By.xpath("//a[contains(@href,'clouddrive')]"));

    private final SelenideElement yourAddressesLink = $(By.xpath("//a[contains(@href,'addresses')]"));

}
