package com.amazon.pages.productspages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class SearchResultsPage {

    private final SelenideElement sortingDropdown =  $(By.id("a-autoid-0-announce"));

    private final SelenideElement sortingSelector = $(By.id("s-result-sort-select"));

    private final String PriceDescOption = "price-desc-rank";

    private final ElementsCollection productsSelenideElementList = $$(By.xpath("//div[contains(@class,'s-main-slot')]/div[@data-component-type='s-search-result' and not(contains(@class, 'AdHolder'))]"));

    private final SelenideElement departmentSubCategory = $(By.xpath("//*[@id='departments']//li[contains(@class,'s-navigation-indent-1')]/span/span"));

    private final SelenideElement paginationNextPageBtn = $(By.xpath("//span[contains(@cel_widget_id,'MAIN-PAGINATION')]//li[@class='a-last']"));

    private final SelenideElement paginationHighlightedBtn = $(By.xpath("//span[contains(@cel_widget_id,'MAIN-PAGINATION')]//li[@class='a-selected']"));

    private final SelenideElement needHelpSection = $(By.xpath("//span[contains(@cel_widget_id,'MAIN-FEEDBACK')]"));

    public SelenideElement getLeftMenuBrandCheckboxFor(String brand) {
        return $(By.xpath(String.format("//li[contains(@id,'%s')]//a", brand)));
    }

    public SelenideElement getLeftMenuRatingFilterFor(String rating) {
        return $(By.xpath(String.format("//section[@aria-label='%s Stars & Up']", rating.trim())));
    }

}
