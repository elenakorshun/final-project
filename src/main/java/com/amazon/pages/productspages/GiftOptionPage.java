package com.amazon.pages.productspages;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class GiftOptionPage {

    private final SelenideElement helpContentSection = $(By.className("help-content"));

}
