package com.amazon.pages.productspages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class CartPage {

    private final ElementsCollection cartProductSelenideElementsList = $$(By.xpath("//div[@id='sc-active-cart']//div[@data-item-count]"));

    private final SelenideElement bottomSubtotalLbl = $(By.id("sc-subtotal-label-buybox"));

    private final SelenideElement proceedToCheckOutSubtotalLbl = $(By.id("sc-subtotal-label-activecart"));

    private final SelenideElement cartIsEmptyLbl = $(By.xpath("//div[contains(@class,'sc-your-amazon-cart-is-empty')]"));
}
