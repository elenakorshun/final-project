package com.amazon.pages.productspages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class ProductPage {

    private final SelenideElement productTitle = $(By.id("productTitle"));

    private final SelenideElement addToCartBtn = $(By.id("add-to-cart-button"));

    private final SelenideElement viewYourListBtn = $(By.id("WLHUC_viewlist"));

    private final SelenideElement addToListBtn = $(By.id("add-to-wishlist-button-submit"));

    private final SelenideElement confirmTextLbl = $(By.xpath("//div[contains(@id,'confirm-text')]"));

    private final SelenideElement confirmAddToListTextLbl = $(By.id("WLHUC_result_itemCount"));

    private final SelenideElement videoThumbnail = $(By.xpath("//div[@id='altImages']//li[contains(@class,'videoThumbnail')]"));

    private final SelenideElement videoPlayer = $(By.xpath("//video[contains(@id,'detailpage-imageblock-player')]"));

    private final SelenideElement videoLikeIcon = $(By.xpath("//div[@id='vse-video-votes-lb-details-vse_ib_iv']//i[@data-element-id='video-vote-positive-icon']"));

    private final SelenideElement videoLikeCounter = $(By.xpath("//div[@id='vse-video-votes-lb-details-vse_ib_iv']//span[@data-element-id='video-vote-positive-count']"));

    private final ElementsCollection photoIconsForProduct = $$(By.xpath("//div[@id='altImages']//li[contains(@class,'imageThumbnail')]")).excludeWith(attribute("class", "videoThumbnail"));

    private final SelenideElement ProductPhotoImgElement = $(By.xpath("//div[@id='main-image-container']//li[contains(@class,'selected')]//img"));

    private final SelenideElement ProductPhotoElementToTakeScreenshot = $(By.xpath("//div[@id='main-image-container']//li[contains(@class,'selected')]//div"));

    public SelenideElement getShareIconFor(String socialNetwork){
        return $(By.id(socialNetwork));
    }
}
