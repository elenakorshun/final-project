package com.amazon.pages.productspages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

@Getter
public class CustomListPage {

    private final ElementsCollection customListProducts = $$(By.xpath("//ul[@id='g-items']//li"));

    private final SelenideElement productsGridContainer = $(By.xpath("//div[contains(@id,'g-items')]"));

    private final SelenideElement gridViewSwitcher = $(By.id("grid-view-switcher"));

    private final SelenideElement listViewSwitcher =  $(By.id("list-view-switcher"));

    private final SelenideElement shareListToOthersLink =  $(By.id("shareWidgetContainer"));

    private final SelenideElement viewOnlyBtn =  $(By.name("invite-view-only-button"));

    private final SelenideElement copyLinkForViewOnlyLink = $(By.id("invite-view-only-copy"));

    private final SelenideElement relatedItemContainer = $(By.xpath("//*[contains(@id,'CardInstance')]"));

    private final SelenideElement closeInvitePopUpBtn = $(By.xpath("//button[@aria-label='Close']"));

    private final SelenideElement invitePopUp = $(By.xpath("//div[@role='dialog']"));

}
