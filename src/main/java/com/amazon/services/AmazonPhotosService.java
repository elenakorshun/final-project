package com.amazon.services;

import com.amazon.properties.PropertyReader;
import com.amazon.utils.CustomWaiter;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import com.amazon.pages.AmazonPhotosPage;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.File;
import java.io.FileNotFoundException;
import java.time.Duration;
import static com.codeborne.selenide.Selenide.$;
import static com.amazon.properties.PropertyReader.PropertyName.*;

public class AmazonPhotosService extends HeaderMenuService{

    AmazonPhotosPage amazonPhotosPage;
    PropertyReader propertyReader;
    CustomWaiter customWaiter;

    public AmazonPhotosService() {
        amazonPhotosPage = new AmazonPhotosPage();
        propertyReader = new PropertyReader();
        customWaiter = new CustomWaiter();
    }

    public void clickOnBackBtn(){
        amazonPhotosPage.getBackBtn().click();
    }

    public void clickOnAddPhotoBtn(){
        amazonPhotosPage.getAddPhotoBtn().click();
    }

    public void clickOnCloseUploadPopupBtn(){
        amazonPhotosPage.getCloseUploadPopupBtn().shouldBe(Condition.visible,
                Duration.ofMillis(Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime)))).click();
    }

    public void clickOnTrashBinToDoSection(){
         amazonPhotosPage.getTrashBinToDoSection().click();
    }

    public Boolean waitUntilUploadingPopUpIsVisible() {
       return customWaiter.waitForConditionWithoutExcp(amazonPhotosPage.getUploadingPopUp(), Condition.visible);
    }

    public Boolean waitUntilSuccessUploadingPopUpIsVisible(){
        return customWaiter.waitForConditionWithoutExcp(amazonPhotosPage.getSuccessUploadingPopUp(),
                Condition.visible, Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime)));
    }

    public void waitUntilPhotoIsUploaded() {
        clickOnCloseUploadPopupBtn();
        amazonPhotosPage.getFirstPhoto()
                .shouldBe(Condition.visible, Duration.ofMillis(Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime))));
    }

    public void waitUntilDeleteSuccessPopUpVisible(){
        amazonPhotosPage.getDeleteSuccessPopUp().shouldBe(Condition.visible,
                Duration.ofMillis(Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime))));
    }

    public Boolean waitUntilEmptyStateElementIsVisible(){
        return customWaiter.waitForConditionWithoutExcp(amazonPhotosPage.getEmptyStateElement(),
                Condition.visible, Long.parseLong(propertyReader.getPropertyValue(longWaitTime)));
    }

    public Boolean waitUntilFirstPhotoIsVisible(){
        return customWaiter.waitForConditionWithoutExcp(amazonPhotosPage.getPhotosCollection().get(0),
                Condition.visible, Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime)));
    }


    public int getPhotosCollectionSize(){
        return amazonPhotosPage.getPhotosCollection().size();
    }

    public boolean waitForFirstPhotoIsDisplayed(){
        return customWaiter.waitForConditionWithoutExcp(amazonPhotosPage.getPhotosCollection().get(0), Condition.visible);
    }

    public void clickOnPhotoByIndex(int index) {
        amazonPhotosPage.getPhotosCollection().get(index).click();
    }

    public void clickOnConfirmDeleteBtn() {
        amazonPhotosPage.getConfirmDeleteBtn().click();
    }

    public void clickOnExpandableOptionsBtn() {
        amazonPhotosPage.getImageHeadOptionsPanel().hover();
        amazonPhotosPage.getExpandableOptionsBtn().click();
    }

    public void uploadPhoto(File file) {
        amazonPhotosPage.getUploadButton().uploadFile(file);
    }

    public void downloadPhoto() {
        try {
            amazonPhotosPage.getDownloadBtn().download(); // result is ignored because file is downloaded but returned first file from header
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void dragPhotoByIndex(int photoIndex) {
        Selenide.actions()
                .moveToElement(amazonPhotosPage.getPhotosCollection().get(photoIndex))
                .clickAndHold()
                .moveByOffset(10,10) //  offset to any position to make delete bin visible
                .build().perform();
    }


}
