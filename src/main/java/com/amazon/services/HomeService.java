package com.amazon.services;

import com.amazon.properties.PropertyReader;
import com.amazon.utils.CustomWaiter;
import com.amazon.pages.HomePage;

import static com.codeborne.selenide.Condition.visible;
import static com.amazon.properties.PropertyReader.PropertyName.*;

public class HomeService extends HeaderMenuService {

    private final HomePage homePage;
    private CustomWaiter customWaiter;
    private PropertyReader propertyReader;

    public HomeService(){
        homePage = new HomePage();
        customWaiter = new CustomWaiter();
        propertyReader = new PropertyReader();
    }

    public boolean waitUntilFirstGridBlockVisible() {
        return customWaiter.waitForConditionWithoutExcp(homePage.getFirstGridBlock(), visible,
                Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime)));
    }

    public void moveToBackToTopBtn() {
        homePage.getBackToTopBtn().hover();
    }

    public void pressBackToTopBtn() {
        homePage.getBackToTopBtn().click();
    }

    public String getLogoId() {
        return homePage.getMainLogoId();
    }
}
