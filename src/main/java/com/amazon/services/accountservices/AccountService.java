package com.amazon.services.accountservices;

import com.amazon.pages.accountpages.AccountPage;
import com.amazon.services.HeaderMenuService;

public class AccountService extends HeaderMenuService {

    AccountPage accountPage;

    public AccountService(){
        accountPage = new AccountPage();
    }

    public void clickOnManageDriveAndPhotosLink(){
        accountPage.getManageDriveAndPhotosLink().click();
    }

    public void clickOnYourAddressesLink(){
        accountPage.getYourAddressesLink().click();
    }

}
