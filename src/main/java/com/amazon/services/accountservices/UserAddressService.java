package com.amazon.services.accountservices;

import com.amazon.blocks.productsblocks.AddressBlock;
import com.amazon.models.UserAddress;
import com.amazon.pages.accountpages.UserAddressPage;
import com.codeborne.selenide.Condition;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.sleep;

public class UserAddressService {

    private UserAddressPage userAddressPage;

    public UserAddressService(){
        userAddressPage = new UserAddressPage();
    }

    public void clickOnAddAddressBtn(){
        userAddressPage.getAddAddressBtn().click();
    }

    public void clickOnSaveBtn(){
        userAddressPage.getSaveBtn().click();
    }

    public int getAddressesCountExceptResidential() {
        return userAddressPage.getAddresses().size() - 1;
    }

    public  List<AddressBlock> getAddressBlockList(){
        return userAddressPage.getAddresses().stream().map(a -> new AddressBlock(a)).collect(Collectors.toList());
    }

    public void deleteAddressByIndex(int addressIndex) {
        AddressBlock addressBlock = new AddressBlock(userAddressPage.getAddresses().get(addressIndex));
        addressBlock.ClickOnRemoveBtn();
        sleep(1000); // until popup is completely loaded
        userAddressPage.getDeletePopUp().shouldNotHave((Condition.attributeMatching("style", ".*transition: all 500ms linear 0s;*")));
        userAddressPage.getConfirmRemovalForAddressByIndexBtn(addressIndex).click();
    }

    public AddressBlock getAddressBlockByFullName(String fullName) {
        return getAddressBlockList().stream().filter(a -> a.getFullNameText().equals(fullName)).findFirst().orElse(null);
    }

    public void fillInUserAddress(UserAddress userAddress){
        userAddressPage.getCountrySelect().selectOptionByValue(userAddress.getCountryAbbreviation().getOptionToSelectInDropdown());
        userAddressPage.getStateSelect().selectOptionByValue(userAddress.getState());
        if(userAddress.getFullName() != null) {
            userAddressPage.getFullNameTextBox().clear();
            userAddressPage.getFullNameTextBox().sendKeys(userAddress.getFullName());
        }
        userAddressPage.getPhoneNumberTextBox().sendKeys(userAddress.getPhoneNumber());
        userAddressPage.getAddressLine1TextBox().sendKeys(userAddress.getAddressLine1());
        if(userAddress.getAddressLine2() != null) {
            userAddressPage.getAddressLine2TextBox().sendKeys(userAddress.getAddressLine2());
        }
        userAddressPage.getCityTextBox().sendKeys(userAddress.getCity());
        userAddressPage.getPostalCodeTextBox().sendKeys(userAddress.getZipCode());
        if(userAddress.getIsDefaultAddress()){
            userAddressPage.getDefaultAddressCheckbox().click();
        }
    }
}
