package com.amazon.services.accountservices;

import com.amazon.pages.accountpages.LoginPage;
import com.amazon.services.HeaderMenuService;
import com.amazon.utils.Logger;
import io.qameta.allure.Allure;

public class LoginService extends HeaderMenuService {

    private final LoginPage loginPage;

    public LoginService(){
        loginPage = new LoginPage();
    }

    public void enterEmail(String email){
        Allure.step("Enter email");
        loginPage.getEmailInput().sendKeys(email);
        Logger.debug("Email is entered");
        loginPage.getEmailContinueBtn().click();
        Logger.debug("Continue button is clicked on Login page");
    }

    public void enterPassword(String pass){
        Allure.step("Enter password");
        loginPage.getPasswordInput().sendKeys(pass);
        Logger.debug("Password is entered");
        loginPage.getSignInBtn().click();
        Logger.debug("Sign in button is clicked on Login Page");
        Logger.info("User is logged in");
    }

    public void returnToHomePage(){
        loginPage.getAmazonLbl().click();
        Logger.debug("Return to home page button is clicked on");
        Logger.info("Home page is opened");
    }

}
