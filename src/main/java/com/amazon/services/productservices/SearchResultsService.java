package com.amazon.services.productservices;


import com.amazon.blocks.productsblocks.ProductBlock;
import com.amazon.services.HeaderMenuService;
import com.codeborne.selenide.Condition;
import com.amazon.models.PageData;
import com.amazon.pages.productspages.SearchResultsPage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.url;

public class SearchResultsService extends HeaderMenuService {

    private SearchResultsPage searchResultsPage;

    public SearchResultsService(){
        searchResultsPage = new SearchResultsPage();
    }

    public String getDepartmentSubCategoryText(){
        return searchResultsPage.getDepartmentSubCategory().text();
    }

    public List<ProductBlock> getProductBlockListOnCurrentPage(){
        return searchResultsPage.getProductsSelenideElementList().stream().map(ProductBlock::new).collect(Collectors.toList());
    }

    public int getCountOfProductsCurrentPage(){
        return searchResultsPage.getProductsSelenideElementList().size();
    }

    public ProductBlock getProductBlockByIndex(int index) {
        return new ProductBlock(searchResultsPage.getProductsSelenideElementList().get(index));
    }

    public void clickOnPaginationNextPageBtn() {
        searchResultsPage.getPaginationNextPageBtn().click();
    }

    public boolean isPaginationNextPageBtnVisible() {
        return searchResultsPage.getPaginationNextPageBtn().isDisplayed();
    }

    public String getTextOnPaginationHighlightedBtn() {
        return searchResultsPage.getPaginationHighlightedBtn().text();
    }

    public void clickOnSortingDropDown() {
        searchResultsPage.getSortingDropdown().click();
    }

    public void selectPriceDescSorting() {
       searchResultsPage.getSortingSelector().selectOptionByValue(searchResultsPage.getPriceDescOption());
    }

    public List<Double> getProductsPriceOnCurrentPage(){
        return getProductBlockListOnCurrentPage()
                .stream()
                .filter(ProductBlock::isProductHavePrice)
                .map(ProductBlock::getProductPrice)
                .collect(Collectors.toList());
    }

    public List<Double> getProductsRatingOnCurrentPage(){
        return getProductBlockListOnCurrentPage()
                .stream()
                .filter(ProductBlock::isProductHaveRating)
                .map(ProductBlock::getProductRating)
                .collect(Collectors.toList());
    }

    public List<String> getProductsTitleOnCurrentPage(){
        return getProductBlockListOnCurrentPage().stream().map(ProductBlock::getProductTitle).collect(Collectors.toList());
    }

    public void checkBrandCheckboxFor(String brand){
        searchResultsPage.getLeftMenuBrandCheckboxFor(brand).click();
    }

    public void clickOnRatingFilterFor(String rating){
        searchResultsPage.getLeftMenuRatingFilterFor(rating).click();
    }

    public List<Double> getProductsPriceOnAllPages() {
        List<Double> productPricesOnAllPages = new ArrayList<>();
        waitForSearchResultsToLoadCompletely();
        productPricesOnAllPages.addAll(getProductsPriceOnCurrentPage());
        while(MoveToNextPageIfPossible()) {
            productPricesOnAllPages.addAll(getProductsPriceOnCurrentPage());
        }
        return productPricesOnAllPages;
    }

    public List<String> getProductsTitleOnAllPagesList() {
        List<String> productTitlesOnAllPagesList = new ArrayList<>();
        waitForSearchResultsToLoadCompletely();
        productTitlesOnAllPagesList.addAll(getProductsTitleOnCurrentPage());
        while(MoveToNextPageIfPossible()) {
            productTitlesOnAllPagesList.addAll(getProductsTitleOnCurrentPage());
        }
        return productTitlesOnAllPagesList;
    }

    public List<Double> getProductsRatingOnAllPagesList() {
        List<Double> productRatingOnAllPagesList = new ArrayList<>();
        waitForSearchResultsToLoadCompletely();
        productRatingOnAllPagesList.addAll(getProductsRatingOnCurrentPage());
        while(MoveToNextPageIfPossible()) {
            productRatingOnAllPagesList.addAll(getProductsRatingOnCurrentPage());
        }
        return productRatingOnAllPagesList;
    }

    public void waitForSearchResultsToLoadCompletely() {
        searchResultsPage.getNeedHelpSection().shouldBe(Condition.visible);
    }

    public boolean MoveToNextPageIfPossible() {
        if(isPaginationNextPageBtnVisible()) {
            clickOnPaginationNextPageBtn();
            waitForSearchResultsToLoadCompletely(); // wait for all product blocks to load
            return true;
        }
        return false;
    }

    public List<PageData> moveThroughAllAvailablePagesUsingNext() {
        List<PageData> pageDataList = new ArrayList<>();
        waitForSearchResultsToLoadCompletely();
        PageData pageData = new PageData(searchResultsPage.getPaginationHighlightedBtn().text(),
                url(), getProductBlockByIndex(0).getProductTitle());
        pageDataList.add(pageData);
        while(MoveToNextPageIfPossible()) {
            pageData = new PageData(searchResultsPage.getPaginationHighlightedBtn().text(), url(),
                    getProductBlockByIndex(0).getProductTitle());
            pageDataList.add(pageData);
        }
        return pageDataList;
    }
}
