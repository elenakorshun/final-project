package com.amazon.services.productservices;

import com.amazon.utils.CustomWaiter;
import com.amazon.services.HeaderMenuService;
import com.codeborne.selenide.Condition;
import com.amazon.pages.productspages.GiftOptionPage;

public class GiftOptionService extends HeaderMenuService {

    private GiftOptionPage giftOptionPage;
    private CustomWaiter customWaiter;

    public GiftOptionService(){
        giftOptionPage = new GiftOptionPage();
        customWaiter = new CustomWaiter();
    }

    public Boolean waitUntilHelpContentSectionVisible(){
        return customWaiter.waitForConditionWithoutExcp(giftOptionPage.getHelpContentSection(), Condition.visible);
    }
}
