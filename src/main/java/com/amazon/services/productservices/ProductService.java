package com.amazon.services.productservices;

import com.amazon.services.HeaderMenuService;
import com.amazon.utils.CustomWaiter;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.amazon.properties.PropertyReader;
import com.amazon.pages.productspages.ProductPage;
import com.amazon.utils.JSHelper;
import com.amazon.utils.ScreenShotHelper;

import java.awt.image.BufferedImage;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static com.amazon.properties.PropertyReader.PropertyName.shortWaitTime;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class ProductService extends HeaderMenuService {

    private final ProductPage productPage;
    private final PropertyReader propertyReader;
    private final CustomWaiter customWaiter;

    public ProductService(){
        productPage = new ProductPage();
        propertyReader = new PropertyReader();
        customWaiter = new CustomWaiter();
    }

    public String getProductTitle(){
        return productPage.getProductTitle().text();
    }

    public void clickOnShareInSocialNetwork(String socialNetwork) {
        productPage.getShareIconFor(socialNetwork).click();
    }

    public void clickOnAddToCartBtn() {
        productPage.getAddToCartBtn().click();
    }

    public void clickOnAddToListBtn() {
        productPage.getAddToListBtn().click();
    }

    public void clickOnViewYourListBtn() {
        productPage.getViewYourListBtn().click();
    }

    public Boolean waitUntilConfirmTextIsVisible() {
        return customWaiter.waitForConditionWithoutExcp(productPage.getConfirmTextLbl(), Condition.visible);
    }

    public Boolean waitUntilConfirmTextForAddToListIsVisible() {
        return customWaiter.waitForConditionWithoutExcp(productPage.getConfirmAddToListTextLbl(), Condition.visible);
    }

    public void clickOnVideoThumbnail() {
        productPage.getVideoThumbnail().click();
    }

    public void clickOnVideoPlayer() {
        productPage.getVideoPlayer().click();
    }

    public void waitForVideoPlayerVisible() {
        productPage.getVideoPlayer().shouldBe(Condition.visible,
                Duration.ofMillis(Long.parseLong(propertyReader.getPropertyValue(shortWaitTime))));
    }

    public void clickOnVideoLikeIcon() {
        productPage.getVideoLikeIcon().click();
    }

    public String getVideoLikeIconClassValue() {
        productPage.getVideoLikeCounter().shouldNotHave((exactText(""))); // waiting until like/dislike values load
        return productPage.getVideoLikeIcon().getAttribute("class");
    }

    public void runVideo() {
        waitForVideoPlayerVisible();
        JSHelper.runVideo(productPage.getVideoPlayer());
    }

    public int getVideoLikeCount() {
        productPage.getVideoLikeCounter().shouldNotHave((exactText(""))); // waiting until text appear
        return Integer.parseInt(productPage.getVideoLikeCounter().text());
    }

    public BufferedImage getScreeenshotOfVideoPlayer() {
        ScreenShotHelper screenShotHelper = new ScreenShotHelper();
        return screenShotHelper.getScreenshot(productPage.getVideoPlayer());
    }

    public void stopVideo() {
        waitForVideoPlayerVisible();
        JSHelper.stopVideo(productPage.getVideoPlayer());
    }

    public List<BufferedImage> getScreenShotsForEveryPhoto() {
       List<BufferedImage> productPhotos = new ArrayList<>();
       for(SelenideElement photo : productPage.getPhotoIconsForProduct()) {
           productPage.getProductPhotoElementToTakeScreenshot().hover();
           photo.hover();
           productPage.getProductPhotoImgElement().shouldBe(image);
           BufferedImage photoScreenShot = new ScreenShotHelper()
                   .getScreenshot(productPage.getProductPhotoElementToTakeScreenshot());
           productPhotos.add(photoScreenShot);
       }
       return productPhotos;
    }
}
