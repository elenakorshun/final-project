package com.amazon.services.productservices;

import com.amazon.blocks.productsblocks.CustomListProductBlock;
import com.amazon.utils.CustomWaiter;
import com.amazon.services.HeaderMenuService;
import com.codeborne.selenide.Condition;
import com.amazon.pages.productspages.CustomListPage;

import java.util.List;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static org.junit.Assert.assertTrue;


public class CustomListService extends HeaderMenuService {

    private CustomListPage customListPage;
    private CustomWaiter customWaiter;

    private final String ID_VALUE_FOR_GRID_VIEW = "g-items-grid-container";
    private final String invitePopUpStyleAttributeWaitForRegExp = ".*transition.*";

    public CustomListService() {
        customListPage = new CustomListPage();
        customWaiter = new CustomWaiter();
    }

    public CustomListProductBlock getCustomListProductBlockByIndex(int index) {
        return new CustomListProductBlock(customListPage.getCustomListProducts().get(index));
    }

    public int getCustomListProductBlockCount() {
        return customListPage.getCustomListProducts().size();
    }

    public void clickOnGridViewSwitcher() {
        customListPage.getGridViewSwitcher().click();
    }

    public Boolean waitUntilProductGridContainerHasGridView() {
        return customWaiter.waitForConditionWithoutExcp(customListPage.getProductsGridContainer(),
                Condition.attribute("id", ID_VALUE_FOR_GRID_VIEW));
    }

    public void clickOnShareListToOthersLink() {
        customListPage.getShareListToOthersLink().click();
    }

    public void clickOnViewOnlyBtn() {
        customListPage.getInvitePopUp().shouldNotHave(Condition.attributeMatching("style", invitePopUpStyleAttributeWaitForRegExp));
        sleep(1000); // just in case, to rework
        customListPage.getViewOnlyBtn().click();
    }

    public void clickOnCopyLinkForViewOnlyLink() {
        customListPage.getCopyLinkForViewOnlyLink().click();
    }

    public void clickOnCloseInvitePopUpBtn() {
        customListPage.getCloseInvitePopUpBtn().click();
    }

    public void clickOnDeleteButtonForAllItemsFromShoppingList() {
        getCustomListProductBlockListOnCurrentPage().stream().forEach(CustomListProductBlock::clickOnDeleteProductFromListBtn);
    }

    public List<CustomListProductBlock> getCustomListProductBlockListOnCurrentPage(){
        return customListPage.getCustomListProducts().stream().map(CustomListProductBlock::new).collect(Collectors.toList());
    }

}
