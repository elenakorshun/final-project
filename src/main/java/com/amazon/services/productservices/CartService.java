package com.amazon.services.productservices;

import com.amazon.blocks.productsblocks.CartProductBlock;
import com.amazon.properties.PropertyReader;
import com.amazon.services.HeaderMenuService;
import com.amazon.utils.CustomWaiter;
import com.amazon.pages.productspages.CartPage;
import com.codeborne.selenide.Condition;

import static com.amazon.properties.PropertyReader.PropertyName.longWaitTime;
import static com.amazon.properties.PropertyReader.PropertyName.shortWaitTime;
import static com.codeborne.selenide.Selenide.$;

public class CartService extends HeaderMenuService {

    private CartPage cartPage;
    private CustomWaiter customWaiter;
    private final PropertyReader propertyReader;

    public CartService() {
        cartPage = new CartPage();
        customWaiter = new CustomWaiter();
        propertyReader = new PropertyReader();
    }

    public CartProductBlock getCartProductBlockByIndex(int index) {
        return new CartProductBlock(cartPage.getCartProductSelenideElementsList().get(index));
    }

    public int getCountOfCartProducts() {
        return cartPage.getCartProductSelenideElementsList().size();
    }

    public Boolean waitUntilBottomSubtotalLblHasText(String totalItems){
        return customWaiter.waitForConditionWithoutExcp(cartPage.getBottomSubtotalLbl(), Condition.text(totalItems),Long.parseLong(propertyReader.getPropertyValue(longWaitTime)));
    }

    public Boolean waitUntilProceedToCheckOutSubtotalLblHasText(String totalItems){
        return customWaiter.waitForConditionWithoutExcp(cartPage.getProceedToCheckOutSubtotalLbl(), Condition.text(totalItems), Long.parseLong(propertyReader.getPropertyValue(shortWaitTime)));
    }

    public Boolean waitUntilCartIsEmptyLblVisible(){
        return customWaiter.waitForConditionWithoutExcp(cartPage.getCartIsEmptyLbl(), Condition.visible);
    }
}
