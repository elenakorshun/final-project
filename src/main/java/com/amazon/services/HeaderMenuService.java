package com.amazon.services;

import com.amazon.blocks.HeaderMenuBlock;
import com.amazon.properties.PropertyReader;
import com.amazon.utils.CustomWaiter;
import com.amazon.utils.Logger;
import com.codeborne.selenide.Condition;

import static com.amazon.properties.PropertyReader.PropertyName.longWaitTime;
import static com.amazon.properties.PropertyReader.PropertyName.superLongWaitTime;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;

public class HeaderMenuService {

    private HeaderMenuBlock headerMenuBlock;
    private CustomWaiter customWaiter;
    PropertyReader propertyReader;

    public Boolean waitUntilCartCountLblHasText(String totalItems){
        return customWaiter.waitForConditionWithoutExcp(headerMenuBlock.getCartCountLbl(), Condition.text(totalItems), Long.parseLong(propertyReader.getPropertyValue(superLongWaitTime)));
    }

    public HeaderMenuService() {
       headerMenuBlock = new HeaderMenuBlock();
       customWaiter = new CustomWaiter();
       propertyReader = new PropertyReader();
    }

    public void pressAllMenuLink() {
        headerMenuBlock.getAllMenuLink().click();
        sleep(500); // to rework
        //headerMenuBlock.getLeftMenuCanvasBackground().shouldBe(Condition.attribute("class", headerMenuBlock.getLeftMenuCanvasBackgroundClassValue()));
    }

    public void pressComputersLink(){
        headerMenuBlock.getComputersLink().click();
    }

    public void pressCartLink(){
        headerMenuBlock.getCartLink().click();
    }

    public void enterTextToSearchBar(String searchText){
        headerMenuBlock.getSearchInput().sendKeys(searchText);
    }

    public void pressSearchBtn(){
        headerMenuBlock.getSearchBtn().click();
    }

    public void pressScannersLink(){
        headerMenuBlock.getScannersLink().click();
    }

    public String getScannersLinkText(){
        return headerMenuBlock.getScannersLink().text();
    }

    public void hoverOverAccountPanel() {
        headerMenuBlock.getAccountPanel().hover();
        Logger.debug("Account panel is opened");
    }

    public void clickOnAccountPanel() {
        headerMenuBlock.getAccountPanel().click();
    }

    public void pressSignInBtn() {
        headerMenuBlock.getSignInBtn().click();
        Logger.debug("Sign In button is clicked");
    }

    public void pressSignOutBtn() {
        headerMenuBlock.getSignOutBtn().click();
        Logger.debug("Signed out button is pressed");
    }

    public Boolean waitUntilSignInBtnVisible() {
        return customWaiter.waitForConditionWithoutExcp(headerMenuBlock.getSignInBtn(), visible);
    }

    public Boolean waitUntilSignOutBtnVisible() {
        return customWaiter.waitForConditionWithoutExcp(headerMenuBlock.getSignOutBtn(), visible);
    }

    public String getGreetingMessageText() {
        String greetingMessageText = headerMenuBlock.getGreetingMessageLbl().text();
        Logger.debug("Greeting Message Text is displayed as: "+ greetingMessageText);
        return greetingMessageText;
    }

    public void hoverOverLanguagePanel() {
        headerMenuBlock.getLanguagePanel().hover();
    }

    public void pressGermanLanguageLink() {
        headerMenuBlock.getGermanLanguageLink().click();
    }

    public String getTodaysDealsMenuLinkText() {
        return headerMenuBlock.getTodaysDealsMenuLink().text();
    }

    public void clickOnAccountLink() {
        headerMenuBlock.getAccountLink().click();
    }

    public void clickOnShoppingListLinkBtn() {
        headerMenuBlock.getShoppingListLinkBtn().click();
    }
}
