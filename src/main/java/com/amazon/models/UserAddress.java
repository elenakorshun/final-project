package com.amazon.models;

import com.amazon.enums.CountryAbbreviation;
import lombok.Getter;

@Getter
public class UserAddress {

    private final CountryAbbreviation countryAbbreviation;

    private final String fullName;
    private final String phoneNumber;
    private final String addressLine1;
    private final String addressLine2;
    private final String city;
    private final String state;
    private final String zipCode;
    private final Boolean isDefaultAddress;

    public UserAddress(CountryAbbreviation countryAbbreviation, String fullName, String phoneNumber, String addressLine1, String addressLine2, String city, String state, String zipCode, Boolean isDefaultAddress) {
        this.countryAbbreviation = countryAbbreviation;
        this.fullName = fullName;
        this.phoneNumber = phoneNumber;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.state = state;
        this.zipCode = zipCode;
        this.isDefaultAddress = isDefaultAddress;
    }

}