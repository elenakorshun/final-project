package com.amazon.models;

import lombok.Getter;

@Getter
public class PageData {

    String pageNumber;
    String pageUrl;
    String firstProductTitle;

    public PageData(String pageNumber, String pageUrl, String firstProductTitle) {
        this.pageNumber = pageNumber;
        this.pageUrl = pageUrl;
        this.firstProductTitle = firstProductTitle;
    }
}
