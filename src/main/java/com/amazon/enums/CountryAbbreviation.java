package com.amazon.enums;

public enum CountryAbbreviation {

    US("US"),
    GB("GB"),
    UZ("UZ");
    // not full list

    private String optionToSelectInDropdown;

    CountryAbbreviation(String optionToSelectInDropdown) {
        this.optionToSelectInDropdown = optionToSelectInDropdown;
    }

    public String getOptionToSelectInDropdown(){
        return optionToSelectInDropdown;
    }
}
