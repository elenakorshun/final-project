package com.amazon.states;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressState {

    private String firstlyAddedAddressFullName = "";

    private String lastlyAddedAddressFullName = "";
}
