package com.amazon.states;

import lombok.Getter;
import lombok.Setter;
import com.amazon.models.PageData;

import java.util.List;

@Getter
@Setter
public class SearchResultsState {

    private List<PageData> pageDataList;

    private String subCategoryText = "";
}
