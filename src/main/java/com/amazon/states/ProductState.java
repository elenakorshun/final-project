package com.amazon.states;

import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.util.List;

@Getter
@Setter
public class ProductState {

    private String productTitle = "";

    private int videoLikeCounter;

    private List<BufferedImage> productPhotos;
}
