package com.amazon.states;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrowserState {

    private int countOfOpenedWindows;
}
