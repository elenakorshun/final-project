package com.amazon.utils;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;

public class ClipBoardHelper {

    public String getCopiedValueFromClipBoard(){
        Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            return c.getData(DataFlavor.stringFlavor).toString();
        } catch (UnsupportedFlavorException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
