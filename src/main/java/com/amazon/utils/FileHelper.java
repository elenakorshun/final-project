package com.amazon.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileHelper {

    public void deleteDirectory(String dirPath){
        try {
            FileUtils.deleteDirectory(new File(dirPath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean isFilePresentInDirectory(String fileName, String dirPath) {
        Boolean isFound = false;
        try {
            Stream<Path> walkStream = Files.walk(Paths.get(dirPath));
            isFound = walkStream.filter(p -> p.toFile().isFile()).anyMatch(f ->  f.toString().endsWith(fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isFound;
    }

}
