package com.amazon.utils;

import com.amazon.properties.PropertyReader;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.amazon.properties.PropertyReader.PropertyName.shortWaitTime;

public class CustomWaiter {

    PropertyReader propertyReader;

    public CustomWaiter(){
        propertyReader = new PropertyReader();
    }

    public boolean waitForConditionWithoutExcp(SelenideElement e, Condition expectedCondition, long milliSeconds) {
        try {
            e.shouldHave(expectedCondition, Duration.ofMillis(milliSeconds));
            Logger.debug("Custom waiter is waited for expected Condition");
            return true;
        } catch (Error th) {
            Logger.debug("Custom waiter is not waited for expected Condition");
            return false;
        }
    }

    public boolean waitForConditionWithoutExcp(SelenideElement e, Condition expectedCondition) {
        return waitForConditionWithoutExcp(e, expectedCondition, Long.parseLong(propertyReader.getPropertyValue(shortWaitTime)));
    }

}
