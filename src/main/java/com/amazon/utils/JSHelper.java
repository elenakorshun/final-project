package com.amazon.utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;

import static com.codeborne.selenide.Selenide.executeJavaScript;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class JSHelper {

    public static boolean isElementInViewPortById(String elementId)
    {
        String js = "var element = document.getElementById('" + elementId + "'); " +
                "const rect = element.getBoundingClientRect();" +
                " return (\n" +
                "        rect.top >= 0 &&\n" +
                "        rect.left >= 0 &&\n" +
                "        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&\n" +
                "        rect.right <= (window.innerWidth || document.documentElement.clientWidth)\n" +
                "    );";
        return executeJavaScript(js);
    }

    public static void runVideo(SelenideElement videoEl)
    {
        JavascriptExecutor jse = (JavascriptExecutor)getWebDriver();
        jse.executeScript("arguments[0].play()", videoEl);
    }

    public static void stopVideo(SelenideElement videoEl)
    {
        JavascriptExecutor jse = (JavascriptExecutor)getWebDriver();
        jse.executeScript("arguments[0].pause()", videoEl);
    }
}
