package com.amazon.utils;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.JavascriptExecutor;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.comparison.ImageDiff;
import ru.yandex.qatools.ashot.comparison.ImageDiffer;
import ru.yandex.qatools.ashot.coordinates.WebDriverCoordsProvider;
import ru.yandex.qatools.ashot.shooting.ShootingStrategies;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class ScreenShotHelper {

    private final String PROJECT_DIR = System.getProperty("user.dir");

    AShot aShot;

    public ScreenShotHelper(){
        final Long devicePixelRatio = (Long) ((JavascriptExecutor) getWebDriver()).executeScript("return window.devicePixelRatio;");
        aShot = new AShot()
                .shootingStrategy(
                        ShootingStrategies.viewportPasting(
                                ShootingStrategies.scaling(
                                        Float.valueOf(devicePixelRatio + "f")
                                ), 100)
                )
                .coordsProvider(new WebDriverCoordsProvider());
    }

    public BufferedImage getScreenshot(SelenideElement elementToScreenShot) {
        Screenshot screenshot = aShot.takeScreenshot(getWebDriver(), elementToScreenShot);
        //saveToFile(screenshot.getImage());
        return screenshot.getImage();
    }

    public void saveToFile(BufferedImage saveToFile) {
        int random = new Random().nextInt();
        try {
            ImageIO.write(saveToFile, "PNG", new File(PROJECT_DIR + "/image" + random + ".png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isImagesDifferent(BufferedImage imageA, BufferedImage imageB) {
        ImageDiffer imgDiff = new ImageDiffer();
        ImageDiff diff = imgDiff.makeDiff(imageA, imageB).withDiffSizeTrigger(5000);
        return diff.hasDiff();
    }
}
