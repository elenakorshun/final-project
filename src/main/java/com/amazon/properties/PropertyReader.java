package com.amazon.properties;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {

    private final Properties properties;
    private final String PROJECT_DIR = System.getProperty("user.dir");
    private final String propertyFilePath= "/src/test/resources/properties/Configuration.properties";

    public enum PropertyName {

        shortWaitTime,
        longWaitTime,
        superLongWaitTime,
        homePageUrl,
        pageLoadTimeout,
        directoryWithFilesToUpload,
        directoryWithDownloadedFiles,
        fileNameToUpload,
        partOfGiftOptionHelpPageUrl,
        partOfSignInPageUrl,
        userEmail,
        userPassword
    }

    public PropertyReader(){
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(PROJECT_DIR + propertyFilePath));
            properties = new Properties();
            try {
                properties.load(reader);
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new RuntimeException("Configuration.properties not found at " + PROJECT_DIR + propertyFilePath);
        }
    }

    public String getPropertyValue(PropertyName propertyName) {
        String propertyValue = properties.getProperty(propertyName.toString());
        if(propertyValue != null) {
            return propertyValue;
        }
        else throw new RuntimeException(propertyName + " is not specified in the Configuration.properties file.");
    }

}