package com.amazon.blocks;

import com.codeborne.selenide.SelenideElement;
import lombok.Getter;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

@Getter
public class HeaderMenuBlock {

    private final SelenideElement accountPanel = $(By.id("nav-link-accountList"));

    private final SelenideElement signInBtn = $(By.id("nav-flyout-ya-signin"));

    private final SelenideElement signOutBtn = $(By.id("nav-item-signout"));

    private final SelenideElement greetingMessageLbl = $(By.id("nav-link-accountList-nav-line-1"));

    private final SelenideElement languagePanel =  $(By.id("icp-nav-flyout"));

    private final SelenideElement germanLanguageLink =   $(By.xpath("//div[@id='nav-flyout-icp']//a[contains(@href,'de')]/span"));

    private final SelenideElement todaysDealsMenuLink = $(By.xpath("//div[@id='nav-xshop']//a[contains(@href,'international-sales-offers')]"));

    private final SelenideElement AllMenuLink = $(By.id("nav-hamburger-menu"));

    private final SelenideElement ComputersLink = $(By.xpath("//div[@id='hmenu-content']//a[@data-menu-id='6']/i"));

    private final SelenideElement ScannersLink = $(By.xpath("//div[@id='hmenu-content']//a[contains(@href,'scanners')]"));

    private final SelenideElement searchInput = $(By.id("twotabsearchtextbox"));

    private final SelenideElement searchBtn = $(By.id("nav-search-submit-button"));

    private final SelenideElement cartLink = $(By.id("nav-cart"));

    private final SelenideElement cartCountLbl = $(By.id("nav-cart-count"));

    private final SelenideElement AccountLink = $(By.xpath("//a[contains(@href,'AccountFlyout')]"));

    private final SelenideElement leftMenuCanvasBackground = $(By.id("hmenu-canvas-background"));

    private final String leftMenuCanvasBackgroundClassValue = "hmenu-dark-bkg-color hmenu-opaque";

    private final SelenideElement ShoppingListLinkBtn = $(By.id("nav-flyout-wl-items")).$(By.tagName("a"));
}
