package com.amazon.blocks.productsblocks;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class AddressBlock {

    public AddressBlock(SelenideElement root) {
        this.root = root;
        removeBtn = root.$(By.xpath(".//*[contains(@id,'ya-myab-address-delete-btn')]"));
        editBtn = root.$(By.xpath(".//*[contains(@id,'ya-myab-address-edit-btn')]"));
        fullName = root.$(By.id("address-ui-widgets-FullName"));
        setAsDefaultLink = root.$(By.xpath(".//*[contains(@id,'ya-myab-set-default-shipping-btn')]"));
    }

    private SelenideElement root;

    private SelenideElement removeBtn;

    private SelenideElement editBtn;

    private SelenideElement fullName;

    private SelenideElement setAsDefaultLink;

    public void ClickOnRemoveBtn(){
        removeBtn.click();
    }

    public void ClickOnEditBtn(){
        editBtn.click();
    }

    public String getFullNameText(){
        return fullName.text();
    }

    public SelenideElement getSetAsDefaultLink(){
        return setAsDefaultLink;
    }
}
