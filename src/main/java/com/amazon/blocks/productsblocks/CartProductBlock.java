package com.amazon.blocks.productsblocks;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CartProductBlock {

    public CartProductBlock(SelenideElement root) {
        this.root = root;
        cartProductTitle = root.find(By.xpath(".//span[contains(@class,'sc-product-title')]"));
        quantitySelector = root.$(By.name("quantity"));
        quantityDropdown = root.$(By.xpath(".//span[@data-a-class='quantity']"));
        deleteBtn = root.$(By.xpath(".//input[@data-action='delete']"));
        learnMoreLink = root.$(By.xpath(".//a[@target='AmazonHelp']"));
    }

    private SelenideElement root;

    private final SelenideElement cartProductTitle;

    private SelenideElement quantitySelector;

    private SelenideElement quantityDropdown;

    private SelenideElement deleteBtn;

    private SelenideElement learnMoreLink;

    public void setProductQuantity(String quantity){
        quantitySelector.selectOptionByValue(quantity);
        quantityDropdown.click();
    }

    public String getCartProductTitleText(){
        return cartProductTitle.text();
    }

    public void ClickOnLearnMoreLink(){
        learnMoreLink.click();
    }

    public void ClickOnDeleteBtn(){
        deleteBtn.click();
    }

}
