package com.amazon.blocks.productsblocks;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class CustomListProductBlock {

    public CustomListProductBlock(SelenideElement root) {
        this.root = root;
        customListProductTitle = root.$(By.xpath(".//h3/a"));
        deleteProductFromListBtn = root.$(By.xpath(".//input[@name='submit.deleteItem']"));
        productImg = root.$(By.xpath(".//div[contains(@id,'itemImage')]"));
        moveProductFromListBtn = $(By.xpath("//span[contains(@id, 'move-to-list-button')]"));
    }

    private final SelenideElement customListProductTitle;

    private final SelenideElement deleteProductFromListBtn;

    private final SelenideElement moveProductFromListBtn;

    private final SelenideElement productImg;

    private SelenideElement root;

    public String getCustomListProductTitle(){
        return customListProductTitle.getAttribute("title");
    }

    public void clickOnDeleteProductFromListBtn(){
        deleteProductFromListBtn.click();
    }

    public SelenideElement getProductImg(){
        return productImg;
    }

    public SelenideElement getMoveProductFromListBtn(){
        return moveProductFromListBtn;
    }

    public SelenideElement getDeleteProductFromListBtn(){
        return deleteProductFromListBtn;
    }
}
