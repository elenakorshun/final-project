package com.amazon.blocks.productsblocks;


import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

public class ProductBlock {

    public ProductBlock(SelenideElement root) {
        titleLbl = root.$(By.xpath(".//h2/a/span"));
        priceWholePartLbl = root.$(By.xpath(".//span[@class='a-price-whole']"));
        priceFloatPartLbl = root.$(By.xpath(".//span[@class='a-price-fraction']"));
        ratingLbl = root.$(By.xpath(".//span[contains(@aria-label, 'out of 5 stars')]"));
    }

    private final SelenideElement titleLbl;

    private final SelenideElement ratingLbl;

    private final String ratingValueAttribute = "aria-label";

    private final SelenideElement priceWholePartLbl;

    private final SelenideElement priceFloatPartLbl;

    public void clickOnProduct() {
        titleLbl.click();
    }

    public String getProductTitle() {
        return titleLbl.text();
    }

    public double getProductRating() {
        String ratingAttributeVal = ratingLbl.getAttribute(ratingValueAttribute);
        return Double.parseDouble(ratingAttributeVal.replace(" out of 5 stars", ""));
    }

    public double getProductPrice() {
        String price = String.format("%s.%s", priceWholePartLbl.text(), priceFloatPartLbl.text());
        return Double.parseDouble(price.replaceAll(",",""));
    }

    public boolean isProductHavePrice(){
        return priceWholePartLbl.isDisplayed();
    }

    public boolean isProductHaveRating(){
        return ratingLbl.isDisplayed();
    }

}
